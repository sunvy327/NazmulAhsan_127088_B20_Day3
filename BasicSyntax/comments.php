<?php

// There are 3 types of commenting mainly
// This is the example of a single line comment.
// I used this to cancel a single line. 

?>


<?php

/*This is the example of another type of commenting*/
/*
This can be used for multiple lines.
When the asterisk is ended,
the comment will be ended.*/

/*
This is very useful 
when you need to reduce 
a big number of code
and testing the whole program. 
*/

?>

<?php

#This looks like Twitter.
##No matter in what number you are using hashtag, it still works and 
### I don't understand why !! 
####This works only for the single line.
?>