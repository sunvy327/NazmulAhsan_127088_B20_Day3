<?php
echo "Hello World."."</br>"."I know these things."."</br>";
?>

<?php
echo "These are basics. "."Still I am doing some homework"."</br>";
?>

<?php 
echo "There are ". 3 ." Types of tags here available"."</br>";
?>

<?php 
echo "</br>";
?>

<?
echo "This is written in another short tag. Though I have to enable it.</br>";
?>

<?
echo "I had to go to php.ini to enable short_tag_style.</br>";
?>

<?
echo "Though I don't like this style. It's kinda awkward and uneasy.</br>";
?>

<?php 
echo "</br>";
?>

<%
echo "This is seriously an asp tag. It shows yellow in notepad++. </br>";
%>

<%
echo "It also shows cyan in the notepad++"."</br>"."I don't know how it will look in git.";
%>

<%
$a=2;
$b=3;
echo $a+$b;
echo "</br>"."Mathematical term works here. I guess it's ok for me at this point.";
%>

<script lang="php">
  echo "</br> This is the line where scripting is used, but the language here is php.";  
</script>    

<script lang="php">
  echo "</br> So basically, you can see 4 types of tags. Short tags are interesting.";  
</script>    

<script lang="php">
  echo "</br>But it is a good practie to use the usual php tag. ";  
</script>    
